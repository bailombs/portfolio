import React from 'react';
import cv from '../assets/cv.pdf';
import Navigation from '../components/Navigation';
const Home = () => {
    return (
        <div className='home'>
            <Navigation />
            <div className='home__content'>
                <div className='home__content--child'>
                    <h1>Developpeur Front et Back</h1>
                    <p>Développeur d'application web full-stack Javacript, spécialité
                        Front-end avec Reactjs, redux, sass et des compétences en
                        backend sur  Nodejs, Express, Mongodb, sql, jwt. <br /> <br />
                        Analyser les besoins fonctionnels et techniques
                        concevoir, modéliser et développer des
                        programmes de haute performance avec des
                        différents Framework en méthodologie d'Agile.
                    </p>
                    <div className='pdf'>
                        <a href={cv} download> Telecharger CV</a>
                    </div>

                </div>
            </div>
        </div>
    );
};

export default Home;