import React, { useMemo } from 'react';
import { portfolioData } from '../data/portfolio.js';
import Card from './Card.js';

const ProjectList = () => {
    const data = useMemo(() => portfolioData, [])
    return (
        <section className='project'>

            {data.map((project, index) => (
                <Card key={index} project={project} />
            ))}
        </section>
    );
};

export default ProjectList;

