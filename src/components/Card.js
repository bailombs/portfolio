import React from 'react';

const Card = ({ project }) => {
    console.log(project);
    return (
        <aside className='project__card'>
            <h2 className='project__card--title'>{project.name}</h2>
            <ul className='project__card--techno'>
                {project.languages?.map((language, index) => (
                    <li key={index} className='language'>{language}</li>
                ))}
            </ul>
            <h3 className='project__card--title-descrip'>Description</h3>
            <p className='project__card--description'>{project.info}</p>
            <div className='project__card--source'>
                <a href={project.source} target="_blank" rel="noopener noreferrer" className='code'>code source</a>
                <a href={project.demo} target="_blank" rel="noopener noreferrer" className='lien'>Demo Live</a>
            </div>
        </aside>
    );
};

export default Card;