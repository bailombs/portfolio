import React from 'react';
import { BiHome } from 'react-icons/bi';
import { BsReceipt } from 'react-icons/bs';
import { NavLink } from 'react-router-dom';
import image from '../assets/bailo.jpg';
const Navigation = () => {
    return (
        <div className='sidebar'>
            <div className='sidebar__id'>
                <div className='sidebar__id--content'>
                    <img src={image} alt="profil" />
                    <h3>Mamadou Bailo Sow</h3>
                </div>
            </div>
            <div className='sidebar__nav'>
                <ul className='sidebar__nav--listwrapper'>
                    <li><BiHome className='icon' /><NavLink to='/' className={(nav) => nav.isActive ? 'nav-active' : ''}> Accueil</NavLink></li>
                    <li> <BsReceipt className='icon' /><NavLink to='/portfolio' className={(nav) => nav.isActive ? 'nav-active' : ''}>Portfolio</NavLink></li>
                </ul>
            </div>
        </div>
    );
};

export default Navigation;