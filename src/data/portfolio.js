export const portfolioData = [

    {
        id: 1,
        name: "Site e-commerce",
        languages: ['Javascript', 'React/Redux', 'Sass'],
        source: 'https://gitlab.com/bailombs/boucherie-debouche',
        info: "Une Application full-stack  d'un site e-commerce en cours de developpement avec les technos :en React, redux/toolkit, Nodejs, Express, MongoDb atlas, mongoose, des composants fonctionnels React, React Router, des Hooks(useState, useEffect,...).",
        demo: 'https://www.boucheriedebouche.fr'
    },
    {
        id: 2,
        name: 'Kasa',
        languages: ['Javascript', 'React', 'Sass'],
        source: 'https://gitlab.com/bailombs/mamadoubailosow_11_26042022',
        info: "Une Application de locations d'appartements developper en Single Page Application avec le framework React, React Router, des Hooks(useState, useEffect, useParams, ...) et responsive. Ses maquettes fourmnies sur Figma et la Data en Json.",
        demo: 'https://bailo-kasa.netlify.app/'
    },
    {
        id: 3,
        name: " Application d'intervention",
        languages: ['Javascript', 'React/Redux', 'Sass', "Php"],
        source: 'https://api-interventions-af357e37e47e.herokuapp.com/api/interventions',
        info: "Une Application full-stack d'interventions dont le backent est  developpé en API RESTful avec Php Symfony et Interconnecté avec un frontend developpé en React, redux/toolkit,  des composants fonctionnels React, React Router, des Hooks(useState, useEffect,...).",
        demo: 'https://interventions-5a43905d6b0c.herokuapp.com/'
    },
    {
        id: 4,
        name: "Système d'Authentification",
        languages: ['Javascript', 'React/Redux', 'Sass'],
        source: 'https://gitlab.com/bailombs/authentification',
        info: "Une Application full-stack de système d'authentification via un formulaire d'inscription et de connexion et acceder à la liste des utiliateurs avec connexion authentifiée via un token. Un système de Gestion d'etat, Developper en React, redux/toolkit, Nodejs, Express, MongoDb atlas, mongoose, des composants fonctionnels React, React Router, des Hooks(useState, useEffect,...).",
        demo: 'https://bailo-auth.herokuapp.com/'
    },
    {
        id: 5,
        name: 'BOUKOUTY',
        languages: ['Javascript', 'React/Redux', 'Sass'],
        source: 'https://gitlab.com/bailombs/gestion-de-stocks',
        info: "Une Application en React de boutique en ligne et gestion de stock en cours de developpement. Utilisant des composants fonctionnels React, React Router, des Hooks(useState, useEffect,...), Redux/toolit RTK,  interagissant aevc une API et effectuant un CRUD",
        demo: 'https://boukouty.herokuapp.com/'
    },

    {
        id: 6,
        name: 'ARGENTBANK',
        languages: ['Javascript', 'React', 'Redux'],
        source: 'https://gitlab.com/bailombs/mamadoubailosow_13_27052022',
        info: "Developpement d'une Application Front-end en React,  en utilisant une Api pour un compte utilisateur bancaire, CRUD, un systeme d'autthentification, un systeme de gestion d'etat avec redux, utilisation swagger editor, axios, JSdoc. sign-in: <<username: tony@stark.com, password: password123>>",
        demo: 'https://bailo-argent-bank.netlify.app/'
    },
    {
        id: 7,
        name: 'SportSee',
        languages: ['javascript', 'React', 'Recharts'],
        source: 'https://gitlab.com/bailombs/mamadoubailosow_12_09052022',
        info: "Developpement d'un tableau de bord d'analytics en React, des calls Api en NodeJs pour recuperer les données à affichées dans les graphiques via  Recharts. La maquette fournie sur Figma, et les Users stories sur Kanban.En y ajoutant la JSDoc et le fichier Readme. ",
        demo: 'https://bailo-sportsee.herokuapp.com/'
    },
    {
        id: 8,
        name: 'HRnet',
        languages: ['javascript', 'React, Redux'],
        source: 'https://gitlab.com/bailombs/mamadoubailosow_14_12062022',
        info: "Convertir une Application de gestion des dossiers de ressources humaines de Jquery en React(Dette Technologique). L'application est developpé avec des composants React, React-hook-form, React-table, modales, Menus. Integre un systeme de gestion d'etat avec Redux et localSotarge,rapport de performance Lighthouse, Recherhe, Tri...",
        demo: 'https://hrnet-production.netlify.app/'

    },
    {
        id: 9,
        name: 'Librarie React',
        languages: ['React', 'sass'],
        source: 'https://gitlab.com/bailombs/mamadoubailosow_14_library_11062022',
        info: "Creation et publication d'un composant React sur npm ( modale) avec Create-react-library. Telecharger la bibliotheque sur npm et Reutiliser.",
        demo: 'https://www.npmjs.com/package/modal-react-sow'
    },
    {
        id: 10,
        name: 'site Photographes',
        languages: ['Javsacript', 'Accessibilité'],
        source: 'http://www.github.com',
        info: "Developper  une  plateforme de Photographe. Mise en place des modales, des menus deroulants, des ligthbox, pattern Factory photos et vidéos sans oublier l'accessibilté et la gestion des evenements avec la souris et clavier.  ",
        demo: 'https://lesphotographes.netlify.app/'
    },
    {
        id: 11,
        name: 'Debuggez et Tester SaaS',
        languages: ['Jest'],
        source: 'https://gitlab.com/bailombs/mamadoubailosow_9_22112021',
        info: "Debugger à travers les outils du navigateur comme la console, l'inspecteur, le debogueur, le Network sur le HTML/CSS, le DOM ou l'API. Tester via Jest, Testing librairie des testes unitaires, d'integrations, End-To-End. Afficher les rapports de Testes Jest et de couverture.",
        demo: ''
    },
    {
        id: 12,
        name: "Les Besoins d'un appli",
        languages: ['Figma', 'Kanban'],
        source: 'https://gitlab.com/bailombs/mamadoubailosow_10_21032022',
        info: "Realiser les besoins fonctions  fonctionels et techniques. Creer une maquette via Figma, faire des uses cases, et Un Backlog sur Notion. Les documents sont disponibles en Pdf sur code source.",
        demo: ''
    },
    {
        id: 13,
        name: 'Les Petits Plats',
        languages: ['Html5', 'css3', 'javascript'],
        source: 'https://gitlab.com/bailombs/mamadoubailosow_7_13092021',
        info: "Developper un site web en Integrant une maquette, des Menus deroulants, des tags et  Developper un algorithme de recherche pour l'ensemble du site web.",
        demo: 'https://petits-plats.netlify.app/'
    },
    {
        id: 14,
        name: 'Game ON',
        languages: ['Html5', 'css', 'Regex'],
        source: 'https://gitlab.com/bailombs/gameon-website-fr',
        info: "Optimiser un site web déjà existant, controles des champs d'un formulaire via les regex, bien placés les images, corriger les bugs.",
        demo: 'https://bailo-game-on.netlify.app/'
    },
    {
        id: 15,
        name: 'OH MyFood',
        languages: ['Sass', 'git'],

        source: 'https://gitlab.com/bailombs/mamadoubailosow_3_05012021',
        info: 'Dynamiser une page web avec des animations css via sass, utilisation de git, CI/CD, des transitions, des Keyframes.',
        demo: 'https://bailombs.gitlab.io/mamadoubailosow_3_05012021'
    },
    {
        id: 16,
        name: 'Reservia',
        languages: ['Html5', 'css3'],
        source: 'https://gitlab.com/bailombs/mamadoubailosow_2_02112020',
        info: 'Integrer une maquette en respectant le web semantique, mise en page, Flex, grid,  le validator w3c',
        demo: 'https://bailombs.gitlab.io/mamadoubailosow_2_02112020/'
    }
]